import json
import glob
import os

from utility.decorators import timer


def remove_dublicate():
	filename = 'data/{}/{}'.format('2020-07-16', 'data.json')
	seen = set()
	result = []
	with open(filename, encoding='utf-8') as file:
		res = json.load(file)
		for k, d in enumerate(res):
			h = d.copy()
			h.pop('Meta')
			h.pop('From')
			h.pop('normed_Message')
			h.pop('processed_Message')
			h.pop('signatureField')
			h.pop('RobotSeeDate_dt')
			h.pop('Date')
			h.pop('Seen')
			try:
				h.pop('Review')
			except:
				pass
			try:
				h.pop('Origin')
			except:
				pass
			h = tuple(h.items())
			if h not in seen:
				result.append(d)
				seen.add(h)
		print('ok')
		print(result)
		
		with open(filename, 'w', encoding='utf-8') as file:
			json.dump(result, file, ensure_ascii=False, indent=4)
		

@timer
def merge_json():

	filename = 'data/{}/'.format('2020-07-16')
	read_files = glob.glob("{}/*.json".format(filename))

	result = []
	c = 0
	for v,f in enumerate(read_files):
		with open(f, 'rb') as file:
			result.append(json.load(file))
			c += 1
			print(c)
		os.remove(f)
	with open("{}/data.json".format(filename), 'w', encoding='utf-8') as file:
		json.dump(result, file, ensure_ascii=False, indent=4)
	
	
	
		
	
remove_dublicate()

